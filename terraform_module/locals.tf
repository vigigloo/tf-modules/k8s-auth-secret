resource "random_string" "secret_name" {
  count   = var.secret_name == null ? 1 : 0
  length  = 8
  upper   = false
  special = false
}

locals {
  passwords_to_generate = [for k, v in var.users : k if v == true]
}

resource "random_password" "default-passwords" {
  count  = length(local.passwords_to_generate)
  length = 24
}

locals {
  users = { for k, v in var.users : k => v == true ? random_password.default-passwords[index(local.passwords_to_generate, k)].result : v }
}

locals {
  secret_name     = var.secret_name == null ? "auth-secret-${random_string.secret_name[0].result}" : var.secret_name
  encrypted_users = { for k, v in local.users : k => base64encode(bcrypt(v)) }
}

locals {
  secret_uri = "${var.namespace}/${local.secret_name}"
}