resource "kubernetes_secret" "users" {
  count = length(local.encrypted_users) > 0 ? 1 : 0
  metadata {
    name      = local.secret_name
    namespace = var.namespace
  }
  binary_data = local.encrypted_users
}