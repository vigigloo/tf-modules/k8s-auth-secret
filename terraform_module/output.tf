output "secret-uri" {
  depends_on = [kubernetes_secret.users]
  value      = local.secret_uri

  description = "Resource identifier for the resulting Kubernetes Secret."
}

output "haproxy-annotations" {
  depends_on = [kubernetes_secret.users]

  value = {
    "haproxy.org/auth-type"   = "basic-auth",
    "haproxy.org/auth-secret" = local.secret_uri
  }

  description = "Annotations for the HAProxy Ingress Controller that can be used on an Ingress to enable basic auth."
}

output "users-with-passwords" {
  sensitive  = true
  depends_on = [kubernetes_secret.users]

  value = local.users

  description = "Mapping of users with all passwords, whether they were generated or not."
}
